# Todos After Seeding Project

+ Set website name in nginx-server.conf, then include it in main nginx config (if applicable)
 + Replace "newsite" x4
+ Customize NetBeans IDE project
 + Change the project name in nbproject/project.xml at Project > Configuration > Name
 + Customize the project properties
+ Customize readme.md
 + A starting point is at the end of the current readme.md; just delete the top part about the seed and Laravel
+ Change license (if applicable) in license file and composer.json
+ Change name, description and keywords in composer.json to match this project
+ Add machine name to development environment in bootstrap/start.php
+ Customize configuration in app/config/*
+ Delete this todo.md file