# Ed Rands' Laravel Project Seed

## About The Laravel Project Seed

After starting a few projects with Laravel, I realized I was repeating myself a lot in my first few steps. This seed is meant to alleviate that problem, so I can get rolling on the actual project fast.

Customization steps are listed in todo.md.

This is meant mostly for my benefit, but if it's helpful to you, you're welcome to start your own Laravel based projects with this seed, or fork it and customize it to your needs.

### Contributing

If you'd like to enhance this project seed, you can help in the typical GitHub way.

Issues and pull requests for the project seed should be filed on the [edrands/laravel-seed](https://github.com/edrands/laravel-seed) repository.

Please keep in mind that since this seed is meant for the way I work, if you'd like customizations specific to the way you work, your best option is to fork this project and modify from there.

If your issue or pull request is related to Laravel itself, please see below about how to contribute to Laravel.

### License

My Laravel Project Seed is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)


## About Laravel

[![Latest Stable Version](https://poser.pugx.org/laravel/framework/version.png)](https://packagist.org/packages/laravel/framework) [![Total Downloads](https://poser.pugx.org/laravel/framework/d/total.png)](https://packagist.org/packages/laravel/framework) [![Build Status](https://travis-ci.org/laravel/framework.png)](https://travis-ci.org/laravel/framework)

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable, creative experience to be truly fulfilling. Laravel attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as authentication, routing, sessions, and caching.

Laravel aims to make the development process a pleasing one for the developer without sacrificing application functionality. Happy developers make the best code. To this end, we've attempted to combine the very best of what we have seen in other web frameworks, including frameworks implemented in other languages, such as Ruby on Rails, ASP.NET MVC, and Sinatra.

Laravel is accessible, yet powerful, providing powerful tools needed for large, robust applications. A superb inversion of control container, expressive migration system, and tightly integrated unit testing support give you the tools you need to build any application with which you are tasked.

### Official Documentation

Documentation for the entire framework can be found on the [Laravel website](http://laravel.com/docs).

### Contributing To Laravel

All issues and pull requests should be filed on the [laravel/framework](http://github.com/laravel/framework) repository.

### License

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)


# Project Title

## About

A description of this project, what it's about, what it does, how it works and why the reader should care.

## Technologies

+ [Laravel Framework](http://laravel.com/)

## Standards

Work on this project should strive to adhere to the following standards and models:

+ PHP:[PSR-2](https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-2-coding-style-guide.md) and therefore [PSR-1](https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-1-basic-coding-standard.md) and [PSR-0](https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-0.md)
+ Git:[Git Branching Model](http://nvie.com/posts/a-successful-git-branching-model/)
+ Versioning:[Semantic Versioning v2.*](http://semver.org/)

## License

This project is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)

Portions of this project may be subject to other copyrights and licenses.
